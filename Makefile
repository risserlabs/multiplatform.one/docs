include mkpm.mk
ifneq (,$(MKPM_READY))
include $(MKPM)/gnu
include $(MKPM)/envcache
include $(MKPM)/mkchain
include $(MKPM)/yarn

export DOCUSAURUS ?= $(call yarn_binary,docusaurus)

ACTIONS += install ##
$(ACTION)/install: $(PROJECT_ROOT)/package.json
	@$(YARN) $(ARGS)
	@$(call done,install)

ACTIONS += build~install ##
BUILD_TARGET := public/index.html
public/index.html:
	@$(call reset,build)
$(ACTION)/build: $(call git_deps,\.([jt]sx?)$$)
	@$(DOCUSAURUS) build
	@$(MV) build public
	@$(call done,build)

.PHONY: start +start
start: | ~install +start ##
+start:
	@$(DOCUSAURUS) start

.PHONY: inc
inc: ##
	@$(NPM) version patch --git=false $(NOFAIL)

.PHONY: clean
clean: ##
	-@$(MKCACHE_CLEAN)
	-@$(GIT) clean -fXd \
		$(MKPM_GIT_CLEAN_FLAGS) \
		$(YARN_GIT_CLEAN_FLAGS) \
		$(NOFAIL)

CACHE_ENVS += \
	DOCUSAURUS

-include $(call actions)

endif
