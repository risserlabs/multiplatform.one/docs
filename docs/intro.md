---
sidebar_position: 1
---

# Intro

The **multiplatform.one** ecosystem consists of two core technologies,
[Multiplatform Framework](/docs/multiplatform-framework) and
[Native Theme UI](/docs/native-theme-ui). Multiplatform Framework and Native Theme UI
solve two distinct but related problems. While both technologies technically can be
used independently, we designed them with each other in mind, and thus the full advantages
of each technology are complete when used together.

![multiplatform.one architecture](img/multiplatform.one.drawio.svg)
