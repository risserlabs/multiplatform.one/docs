---
sidebar_position: 1
---

# Features

![multiplatform.one architecture](../img/multiplatform.one.drawio.svg)

## Support for Existing and Future React Frameworks

Multiplatform Framework is not a replacement for existing stable React frameworks, such as NextJS and Expo.
Instead, it is a meta-framework that sits on top of the implementation framework. Technically,
Multiplatform Framework is useless without an implementation framework.

In multiplatform.one terminology, an implementation framework is called a **platform**. More precisely, an
implementation framework wired up to Multiplatform Framework is called a platform.

This architecture enables Multiplatform Framework to strike the right balance between allowing cross-platform
development and enabling developers to bring the frameworks and tools they love. Furthermore, since Multiplatform
Framework enables any React framework to become a platform, it ensures the multiplatform.one ecosystem will have
the theoretical ability to support future React frameworks and ensures Multiplatform Framework stays relevant as
the React landscape changes.

## Bring Your Components

Technically, Multiplatform Framework does not care what component libraries one uses. However, the underlying platforms
(implementation frameworks) must support the component library used. Thus, there are limited options when choosing a
component library if you wish to take full advantage of the cross-platform objective.

The recommended component library is Native Theme UI since we explicitly built it for use with Multiplatform Framework.
Currently, it supports iOS, Android, and web-based platforms such as Electron, Browser Extensions, and of course, websites.
I will get into more details about Native Theme UI later.

Any component library built on React Native, such as Native Base and React Native Paper, should work for most platforms.
Still, support for these component libraries is not official.

## Enforce Cross-Platform Modular Development

Multiplatform is much more than simply a way to build cross-platform applications. Facilitating best development
practices, such as modular and decoupled patterns, is central to the framework.

Modular development is accomplished through isolated component development using Storybook. While this is certainly
not the first framework to support Storybook for the web, it is one of the only frameworks that currently supports
Storybook for native and web platforms.

## Robust Testing Capabilities

### Unit Tests with Jest

Unit testing React components with Jest works out of the box with the Multiplatform Framework.

### Visual Regression Tests with Loki

Unit tests are a great way to test complex and fragile areas of an application. Still, they are not
necessarily required for every line of code, especially if one tries to strike a balance between
productivity and testing.

Visual Regression tests are a way to automatically detect regressions in a graphical interface without
writing any tests. While they don’t fully replace the need for all unit tests, they certainly reduce
the number of required unit tests. They are an ideal way to increase development efficiency without
sacrificing application and component testing.

The Multiplatform Framework has out-of-the-box support for Loki visual regression tests.

## Additional features

### Cross Platform Routing

Routing works differently depending on the platform. The Multiplatform Framework provides a routing
system using [Solito](https://solito.dev) that provides shared routing logic between platforms.

### Visual Studio Code Integration

The Multiplatform Framework is integrated with visual studio, leveraging features such as tasks
and launch settings.

### Linting and Spellchecking

The Multiplatform Framework includes linting and spellchecking the code.
