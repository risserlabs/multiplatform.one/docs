---
sidebar_position: 2
---

# Build System

> _a great **build system** serves as a form of **living documentation**_

The build system is probably the most controversial part of the Multiplatform Framework and thus
requires a detailed explanation. First, it helps to understand what a build system is and why Multiplatform
Framework needs a build system. A **build system** is a set of scripts that automate tasks required
during the development lifecycle of an application. A build system also helps to standardize the
development workflow.

Without a build system, developers are often left with a set of unrelated commands and complex
tasks that require deep knowledge of the underlying systems. These tasks become very
challenging to keep track of and thus require documentation. Unfortunately, rarely enough effort
is allocated for documentation, and the complex instructions tend to slip out of date. Then it
becomes very challenging to onboard new developers and even runs the risk of existing developers
forgetting essential details of the development lifecycle.

A build system transfers knowledge of the development lifecycle out of documentation and the minds
of a few developers and into a set of standardized scripts. A development workflow using a build
system becomes dependent on the build system, forcing the build system to be accurate and
up to date. One could say a genuinely great build system serves as a form of living documentation.
While any application can benefit from a build system, the more complex a development workflow becomes,
the more critical it becomes to implement a build system. Since Multiplatform Framework is a
highly complex development workflow because of all the platforms and development features it supports,
a build system is a core requirement.

Unfortunately, build systems tend to be opinionated, and supporting more than one build system for a
project is not feasible. Thus, choosing the build system is, by definition, a controversial endeavor.

The best thing to do is to choose a build system that does the job well while explaining the decision.

## Ideal Features

The following is a set of features ideal in a build system.

#### Flexible

A build system should be flexible enough to work with any tech stack or language. In other words, it should
be agnostic regarding the technology stack.

#### Common Capabilities

A build system should have first-class support for capabilities commonly used in development tasks,
such as configuration, filesystem operations, text manipulation, process management,
and io (stdin, stdout, stderr).

#### Concise

A build system should ideally enable the development of powerful scripts in a succinct syntax. A
concise syntax prevents developers from getting bogged down in the development of the build system.

#### Unobtrusive

A build system should not get in the way of a developer. It should make a developer's workflow
easier, not harder.

#### Cross-Platform

A build system should work on as many operating systems as possible.

#### Dependency Hierarchy

A build system should support a graph of dependencies to ensure tasks execute in the proper order.

#### Caching

A build system should support basic caching mechanisms to skip tasks that are up to date.

#### Package Management

A build system should support packages to abstract and extend the functionality.

## Solution

### Unix Shell

Comparing and contrasting every existing build system would not be feasible. However, it is
easy to rule out some. For example, one can rule out [Apache Ant](https://ant.apache.org),
[Gradle](https://gradle.org), [Grunt](https://gruntjs.com), and [Gulp](https://gulpjs.com)
simply because they are not [flexible](#flexible) build systems. In other words, they are
build systems meant for specific technology stacks and cannot easily be generalized for
any technology.

In my opinion, the best build system is a [Unix Shell](https://en.wikipedia.org/wiki/Unix_shell). Almost all
development environments interface with a Unix Shell, making it an extremely [flexible](#flexible) choice. A Unix Shell
also has fantastic support for the [common capabilities](#common-capabilities) a build system requires. For example, it
supports environment variables for configuration, and it has very widely known commands for filesystem operations, text
manipulation, process management, and io (stdin, stdout, stderr). Additionally, the Unix Shell can do a lot with very few
commands making it is a highly [concise](#concise) choice. It is also a relatively [unobtrusive](#unobtrusive) choice since
most developers are already familiar with a Unix Shell, and likely their system already has it. The Unix Shell is also
[cross-platform](#cross-platform). The only widely used operating system that does not have a Unix Shell is Windows, and
even Windows has support for a Unix Shell with the advent of
[WSL (Windows Subshell for Linux)](https://learn.microsoft.com/windows/wsl).

### GNU Make

The only features missing from the Unix Shell include a [dependency hierarchy](#dependency-hierarchy), [caching](#caching)
support, and [package management](#package-management). We solve the [dependency hierarchy](#dependency-hierarchy) and
[caching](#caching) with Makefiles. Makefiles have a feared reputation, but they are relatively simple. They are essentially
a cached dependency graph wrapped around a shell. The majority of a Makefile is just basic shell scripting. Makefiles leverage
the filesystem for the cached dependency graph, which is the most powerful yet simple aspect of Makefiles.

Make is one of the most widespread build systems in existence. It was created in 1976 at Bell Labs and is used in the underlying
build system of several major programs and operating systems, including [Linux](https://www.kernel.org) and
[Firefox](https://en.wikipedia.org/wiki/Firefox). It is so prolific that there are several meta-build systems built on top of Make,
such as [CMake](https://cmake.org) and the [GNU Build System](https://en.wikipedia.org/wiki/GNU_Autotools).

However, the devil's in the flavor. There are also several implementations of Unix Shells and Make. Some of the implementations
are not even cross-platform or have licensing issues. To ensure a consistent implementation that is cross-platform, we have narrowed
the build system to use the [GNU Toolchain](https://www.gnu.org), which includes an implementation of Make called
[GNU Make](https://www.gnu.org/software/make) and implementations of various additional utilities, such as
[grep](https://www.gnu.org/software/grep) and [sed](https://www.gnu.org/software/sed).

Many utilities in the [GNU Toolchain](https://www.gnu.org) have a more extensive API than specified by [POSIX](https://en.wikipedia.org/wiki/POSIX).
Thus, if one uses alternative implementations, there could be unexpected problems. Since OSX uses the BSD implementations of sed,
grep, and awk, developers will likely need to install the GNU Toolchain on a Mac. Fortunately, most Linux operating systems, including
WSL on Windows, already come with most or all of the GNU Toolchain.

### MKPM

Make with a Unix Shell almost form the perfect build system, but it is missing a critical feature, [package management](#package-management).
Because GNU Make was created before the [internet age](https://en.wikipedia.org/wiki/Internet_age), package management was not really a mature
concept. [MKPM (Makefile Package Manager)](https://mkpm.dev) adds package management support to GNU Make. However, it is essential to point out
that [MKPM](https://mkpm.dev) is only compatible with GNU Make version 4 and requires several parts of the [GNU Toolchain](https://www.gnu.org),
including [awk](https://www.gnu.org/software/awk), [grep](https://www.gnu.org/software/grep), and [sed](https://www.gnu.org/software/sed).

## Additional Links

- https://mkpm.dev
- https://en.wikipedia.org/wiki/List_of_build_automation_software
- https://en.wikipedia.org/wiki/POSIX
- https://en.wikipedia.org/wiki/Unix_shell
- https://en.wikipedia.org/wiki/GNU_toolchain
- https://www.gnu.org
- https://www.gnu.org/software/make
