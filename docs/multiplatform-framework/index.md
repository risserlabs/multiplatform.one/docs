---
sidebar_position: 2
---

# Multiplatform Framework

Multiplatform Framework provides a standard way to build and deploy cross-platform applications.
It is a universal, reusable software environment for development targeting multiple platforms.
