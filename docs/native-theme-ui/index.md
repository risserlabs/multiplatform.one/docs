---
sidebar_position: 3
---

# Native Theme UI

Native Theme UI is a cross-platform component library that implements the
[Theme UI](https://theme-ui.com) specification. In addition, it uses
[Dripsy](https://www.dripsy.xyz) to implement theming and
[Moti](https://moti.fyi) for animation support.
