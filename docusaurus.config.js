// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "multiplatform.one",
  tagline:
    "an ecosystem that enables development for multiple platforms using one codebase",
  url: "https://multiplatform.one",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: "risserlabs", // Usually your GitHub org/user name.
  projectName: "multiplatform.one", // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          editUrl:
            "https://gitlab.com/risserlabs/multiplatform.one/docs/tree/main/",
        },
        blog: {
          showReadingTime: true,
          editUrl:
            "https://gitlab.com/risserlabs/multiplatform.one/docs/tree/main/",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "multiplatform.one",
        logo: {
          alt: "multiplatform.one",
          src: "img/logo.svg",
        },
        items: [
          {
            type: "doc",
            docId: "intro",
            position: "left",
            label: "Docs",
          },
          {
            href: "https://native-theme-ui.multiplatform.one",
            label: "Native Theme UI Components",
            position: "left",
          },
          {
            href: "https://gitlab.com/risserlabs/multiplatform.one",
            label: "GitLab",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Community",
            items: [
              {
                label: "GitLab",
                href: "https://gitlab.com/risserlabs/multiplatform.one",
              },
              {
                label: "Stack Overflow",
                href: "https://stackoverflow.com/questions/tagged/multiplatform-one",
              },
            ],
          },
          {
            title: "Multiplatform Framework",
            items: [
              {
                label: "Docs",
                href: "/docs/multiplatform-framework",
              },
              {
                label: "Source Code",
                href: "https://gitlab.com/risserlabs/multiplatform.one/multiplatform-framework",
              },
            ],
          },
          {
            title: "Native Theme UI",
            items: [
              {
                label: "Docs",
                href: "/docs/native-theme-ui",
              },
              {
                label: "Source Code",
                href: "https://gitlab.com/risserlabs/multiplatform.one/native-theme-ui",
              },
              {
                label: "Components",
                href: "https://native-theme-ui.multiplatform.one",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Risser Labs LLC`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
